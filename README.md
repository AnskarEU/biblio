# biblio

## Base de dades per formalitzar préstecs en una biblioteca pròpia.

Es fa servir [django](https://www.djangoproject.com) com a framework web i [sqlite3](https://www.sqlite.org) per la base de dades. Com que és configurable es podrà triar la base que més còmode sigui.

Només presento apps de django, pel que s'haurà de començar el projecte amb el nom triat i afegir les carpetes dins la carpeta del projecte que es crea. Un cap desacarregat django:

    :~$ django-admin startproject <nom del projecte>
    :~$ django-admin startapp <nom de l'app>

S'hauran d'afegir les carpetes dins el directori que es crea amb 'startproject' i posa el nom corresponent de les app desitjades en el fitxer 

    settings.py

que es troba dins 

    ~/<nom del projecte>/<nom del projecte>/

S'han d'afegir només les carpetes que hem iniciat com a app, pel que haurien de tenir el mateix nom.

    INSTALLED_APPS = [
        ...
        <nom de l'app>,
    ]

El l'apartat TEMPLATES també s'hauran d'afegir les rutes corresponents a les carpetes que contenen els fitxers .html de les plantilles a renderitzar

    TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
			os.path.join(BASE_DIR,"<nom del projecte>/templates/"),
			os.path.join(BASE_DIR,"<nom de l'app>/templates/"),
        ]
        ...
    }

Per no tenir un munt de fitxers de plantilla he separat les plantilles principals que renderitzen l'aspecte general de la pàgina i les carpetes de les plantilles pròpies de cada aplicació.

